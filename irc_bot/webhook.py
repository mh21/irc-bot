"""HTTP(S) endpoints."""
from cki_lib import misc
from cki_lib.logger import get_logger
import flask
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

from .alertmanager import craft_notification
from .queue import IRC_PUBLISH_QUEUE

app = flask.Flask(__name__)

misc.sentry_init(sentry_sdk, integrations=[FlaskIntegration()], environment=app.env)

LOGGER = get_logger(__name__)


@app.route('/', methods=['GET'])
def index():
    """Return a nearly empty page."""
    return flask.Response(response='OK', status='200')


@app.route('/message', methods=['POST'])
def message_receiver():
    """Route a generic message to IRC."""
    data = flask.request.get_json()
    message = data['message']
    IRC_PUBLISH_QUEUE.put(message)
    return flask.Response(response='OK', status='200')


@app.route('/alertmanager', methods=['POST'])
def alertmanager_receiver():
    """Route an alertmanager message to IRC."""
    data = flask.request.get_json()
    alertmanager_url = misc.shorten_url(data['externalURL'])
    LOGGER.info('alertmanager %s', data)

    if message := craft_notification(data['alerts'], alertmanager_url, 'resolved'):
        IRC_PUBLISH_QUEUE.put(message)

    if message := craft_notification(data['alerts'], alertmanager_url, 'firing'):
        IRC_PUBLISH_QUEUE.put(message)

    return flask.Response(response='OK', status='200')

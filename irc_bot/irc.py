"""Main IRC bot module."""
import os
import signal
import time

from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.session import get_session
import sentry_sdk

from . import irc_commands
from .queue import IRC_PUBLISH_QUEUE

LOGGER = get_logger('cki.irc_bot.irc')
SESSION = get_session('cki.irc_bot.irc')


class SignalWatcher:
    # pylint: disable=too-few-public-methods
    """Watch SIGINT/SIGTERM signals to allow graceful termination."""

    should_stop = False

    def __init__(self):
        """Initialize the watcher."""
        signal.signal(signal.SIGINT, self.int_term_handler)
        signal.signal(signal.SIGTERM, self.int_term_handler)

    def int_term_handler(self, _, __):
        """Notify that the main loop should terminate."""
        self.should_stop = True


class IrcBot:
    # pylint: disable=too-few-public-methods
    """IRC bot."""

    # pylint: disable=too-many-arguments
    def __init__(self, slack_webhook_url):
        """Initialize the IRC bot."""
        self.slack_webhook_url = slack_webhook_url

        cmd_manager = irc_commands.CommandManager()
        all_cmds = [irc_commands.DadJoke(),
                    irc_commands.GetBeakerQueues(),
                    irc_commands.Ping(),
                    irc_commands.SuccessBot(),
                    irc_commands.StandUp(),
                    irc_commands.CoffeeTime()]
        for cmd in all_cmds:
            cmd_manager.register_cmd(cmd)

        self._send_message("Gitlab webhook bot is online!")

    def _send_message(self, message: str):
        """Post message to Slack using the webhook."""
        webhook_payload = {
            "text": message,
            "unfurl_links": False,
            "unfurl_media": False,
        }
        SESSION.post(self.slack_webhook_url, json=webhook_payload)

    def main(self):
        """IRC bot main loop."""
        signal_watcher = SignalWatcher()
        while not signal_watcher.should_stop:
            try:
                if message := IRC_PUBLISH_QUEUE.get():
                    LOGGER.info('Sending message: %s', message)
                    self._send_message(message)
                    # Send the message at most once per second.
                    time.sleep(1)
            # pylint: disable=broad-except
            except Exception:
                LOGGER.exception('Unable to send message')
                time.sleep(5)

            # Sleep for 0.5 seconds.
            time.sleep(0.5)


def main():
    """Start the IRC bot."""
    misc.sentry_init(sentry_sdk)

    slack_webhook_url = os.environ.get('CKI_SLACK_BOT_WEBHOOK')
    IrcBot(slack_webhook_url).main()


if __name__ == '__main__':
    main()

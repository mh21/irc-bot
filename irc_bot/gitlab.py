# pylint: disable=too-few-public-methods
"""Gitlab objects for irc messages."""
import re

from cki_lib import misc


class MergeRequest:
    """MR Object."""

    def __init__(self, project, obj):
        """Initialize."""
        self.project = project
        self.obj = obj

    def get_ci_status(self):
        """Get the status of the last pipeline on the MR."""
        status = 'UNKN'
        pipeline_status = {
            'canceled': '*CANC*',
            'failed': '*FAIL*',
            'pending': 'PEND',
            'running': 'RUNN',
            'success': 'PASS',
        }

        pipelines = self.obj.pipelines.list(per_page=1)
        if pipelines:
            last_pipeline = pipelines[0]
            status = pipeline_status.get(last_pipeline.status, status)
        return f'[{self.obj.state.upper()}/{status}]'

    def get_file_stats(self):
        """Get statistics about file changes."""
        changes = self.obj.changes(access_raw_diffs=True)
        files = changes['changes_count']
        added = sum(len(re.findall(r'^\+', c['diff'], re.M))
                    for c in changes['changes'])
        removed = sum(len(re.findall('^-', c['diff'], re.M))
                      for c in changes['changes'])
        return f'{files}/+{added}/-{removed}'

    def __str__(self):
        """Return message."""
        details = (
            f'*{self.project.path}!{self.obj.iid}*',
            self.obj.author["username"],
            self.get_file_stats(),
            self.get_ci_status(),
            self.obj.title,
        )
        url = misc.shorten_url(self.obj.web_url)

        return f'{" ".join(details)} - {url}'


class Issue:
    """Issue Object."""

    def __init__(self, project, obj):
        """Initialize."""
        self.project = project
        self.obj = obj

    def __str__(self):
        """Return message."""
        details = (
            f'*{self.project.path}#{self.obj.iid}*',
            self.obj.author["username"],
            f'[{self.obj.state.upper()}]',
            self.obj.title,
        )
        url = misc.shorten_url(self.obj.web_url)

        return f'{" ".join(details)} - {url}'


class Epic:
    """Epic Object."""

    def __init__(self, group, obj):
        """Initialize."""
        self.group = group
        self.obj = obj

    def __str__(self):
        """Return message."""
        details = (
            f'*{self.group.path}&{self.obj.iid}*',
            self.obj.author["username"],
            f'[{self.obj.state.upper()}]',
            self.obj.title,
        )
        url = misc.shorten_url(self.obj.web_url)

        return f'{" ".join(details)} - {url}'

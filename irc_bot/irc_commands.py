"""Handle IRC commands."""
import datetime
from functools import lru_cache
import json
import os
import random
import re
import typing

from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.logger import get_logger
from cki_lib.session import get_session
import gitlab
import yaml

from .gitlab import Epic
from .gitlab import Issue
from .gitlab import MergeRequest

GITLAB_PRIVATE_TOKEN = os.environ.get('GITLAB_PRIVATE_TOKEN')
GITLAB_SNIPPET = os.environ.get('GITLAB_SNIPPET')
OK_EMOJIS = ['👌', '🆗', '🎉', '💃', '😊', '😁', '🥰', '😘', '🤗', '🤭']
TEAM_MEMBERS = os.environ.get('TEAM_MEMBERS', '')
STANDUPS = json.loads(os.environ.get('STANDUPS', '{}'))

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)


def shuffled_members(*, remove: typing.Optional[str] = None) -> str:
    """Return a comma-delimited string with all members in random order."""
    members = sorted(TEAM_MEMBERS.split())
    # Set seed so every call on the same day returns the same order.
    random.Random(datetime.datetime.now().timetuple().tm_yday).shuffle(members)
    return ', '.join(m for m in members if m != remove)


class GetBeakerQueues():
    # pylint: disable=too-few-public-methods
    """Output the current status of the Beaker queues."""

    name = 'queues'
    usage = 'Get the current Beaker queues'
    nargs = 0

    @staticmethod
    def _get_queued_receipes(arch, beaker_url, beaker_user):
        # Get the queued recipes for each arch.
        url = f"{beaker_url}/top-recipe-owners.{arch}"
        resp = SESSION.get(url, stream=True)
        for line in resp.text.split('\n'):
            if line.startswith(beaker_user):
                return line.split()[1]
        return 0

    def run(self, conn, event, _):
        """Process the command."""
        # Get the Beaker URL and error out if it's not set.
        beaker_url = os.environ.get('BEAKER_STATS_URL', None)
        if not beaker_url:
            conn.privmsg(
                event.target,
                '👎 Somebody forgot to set BEAKER_STATS_URL')
            return

        beaker_user = os.environ.get('BEAKER_USER', None)
        if not beaker_user:
            conn.privmsg(
                event.target,
                '👎 Somebody forgot to set BEAKER_USER')
            return

        beaker_host = os.environ.get('BEAKER_HOST', None)
        if not beaker_host:
            conn.privmsg(
                event.target,
                '👎 Somebody forgot to set BEAKER_HOST')
            return

        # Get the total jobs running.
        resp = SESSION.get(f'https://{beaker_host}/jobs/', params={
            "jobsearch-0.table": "Group",
            "jobsearch-0.operation": "is",
            "jobsearch-0.value": "cki",
            "jobsearch-1.table": "Status",
            "jobsearch-1.operation": "is",
            "jobsearch-1.value": "Running",
        })
        running_jobs = re.findall(r"Items found: (\d*)", resp.text)[0]

        # Get queues for each architecture.
        arches = ['aarch64', 'ppc64le', 's390x', 's390x_z13', 'x86_64']
        for idx, arch in enumerate(arches):
            result = self._get_queued_receipes(arch, beaker_url, beaker_user)
            arches[idx] = f"{arch}: {result}"

        details_link = misc.shorten_url(f'https://{beaker_host}/jobs/?'
                                        'jobsearch-0.table=Group&'
                                        'jobsearch-0.operation=is&'
                                        'jobsearch-0.value=cki')

        running_jobs = re.findall(r"Items found: (\d*)", resp.text)[0]
        irc_message = (
            f"🤖 CKI Beaker jobs running: {running_jobs}; "
            f"Queued recipes: {', '.join(arches)} - " +
            details_link
        )

        conn.privmsg(event.target, irc_message)


class DadJoke():
    # pylint: disable=too-few-public-methods
    """Output a dad joke."""

    name = 'dadjoke'
    usage = 'Get a good Dad joke'
    nargs = 0

    @staticmethod
    def run(conn, event, _):
        """Process the command."""
        headers = {'Accept': 'application/json'}
        resp = SESSION.get("https://icanhazdadjoke.com/", headers=headers)
        dad_joke = resp.json()['joke'].replace('\n', ' ')
        conn.privmsg(event.target, f'🤠 {dad_joke}')


class HelpCommand():
    # pylint: disable=too-few-public-methods
    """List all commands that the bot understands."""

    name = 'help'
    usage = 'help'
    nargs = 0

    @staticmethod
    def run(conn, event, _):
        """Process the command."""
        conn.privmsg(event.target, ', '.join(event.cmd_manager.cmds))


class SuccessBot():
    # pylint: disable=too-few-public-methods
    """Add a success message to the success snippet."""

    name = "success"
    usage = "talk about some success we had!"
    nargs = 'n'

    @staticmethod
    def run(conn, event, args):
        """Process the command."""
        instance = get_instance('https://gitlab.cee.redhat.com',
                                token=GITLAB_PRIVATE_TOKEN)
        # Get the current snippet content.
        snippet = instance.snippets.get(GITLAB_SNIPPET)
        old_content = snippet.content().decode('utf-8')

        # Add new content.
        nickname = event.source.split('!')[0]
        new_success_message = ' '.join(args)
        timestamp = datetime.datetime.now().isoformat(sep=' ',
                                                      timespec='seconds')
        new_content = (
            f"* **{nickname}:** {new_success_message} "
            f"*{timestamp}* \n{old_content}"
        )

        # Update the snippet.
        snippet.content = new_content
        snippet.save()

        # Note the success
        conn.privmsg(
            event.target,
            f"{random.choice(OK_EMOJIS)} success noted! "
            f"https://gitlab.cee.redhat.com/snippets/{GITLAB_SNIPPET}"
        )


class CommandManager():
    """Handle the commands the bot understands."""

    def __init__(self):
        """Create a new instance."""
        self.cmds = {}
        self.register_cmd(HelpCommand())

    def register_cmd(self, cmd):
        """Register a new command."""
        self.cmds[cmd.name] = cmd

    def __call__(self, conn, event):
        """Handle a message from IRC."""
        text = event.arguments[0].strip()
        if not text.startswith(conn.ircname):
            return
        args = text.split()

        # Mentioning the bot without any arguments causes it to throw an
        # exception and abruptly quit.
        if len(args) < 2:
            return

        cmd_name = args[1]
        if cmd_name not in self.cmds:
            conn.privmsg(event.target, 'Command not found ¯\\_(ツ)_/¯')
            self.process_command('help', [], conn, event)
            return

        self.process_command(cmd_name, args[2:], conn, event)

    def process_command(self, cmd_name, args, conn, event):
        """Process one command."""
        event.cmd_manager = self
        cmd = self.cmds[cmd_name]
        if cmd.nargs != 'n' and cmd.nargs != len(args):
            conn.privmsg(event.target, f'{cmd.usage}')
            return
        try:
            cmd.run(conn, event, args)
        # pylint: disable=broad-except
        except Exception as exc:
            conn.privmsg(event.target, f'😢, ups: {exc!r}')


class StandUp():
    # pylint: disable=too-few-public-methods
    """Notify the team members of a standup."""

    name = 'standup'
    usage = 'Get standup notification + random order!'
    nargs = 0

    @staticmethod
    def run(conn, event, _):
        """Process the command."""
        weekday = str(datetime.datetime.today().weekday())
        title, url = STANDUPS.get(weekday, ['StandUp', None])
        message = f'🙋 {title}: {shuffled_members()}'
        if url:
            message += f' - {misc.shorten_url(url)}'
        conn.privmsg(event.target, message)


class CoffeeTime():
    # pylint: disable=too-few-public-methods
    """Notify the team members about virtual coffee time."""

    name = 'coffeetime'
    usage = 'Get virtual coffee time notification + random order!'
    nargs = 0

    @staticmethod
    def run(conn, event, _):
        """Process the command."""
        weekday = str(datetime.datetime.today().weekday())
        _, url = STANDUPS.get(weekday, ['StandUp', None])
        message = '🙋 ☕️☕️☕️⏰ Virtual Coffee Time ☕️☕️☕️⏰'
        if url:
            message += f' - {misc.shorten_url(url)}'
        message += f' /cc {shuffled_members()}'
        conn.privmsg(event.target, message)


class Ping():
    # pylint: disable=too-few-public-methods
    """Notify team members about something."""

    name = 'ping'
    usage = 'Notify team members about something'
    nargs = 'n'

    @staticmethod
    def run(conn, event, args):
        """Process the command."""
        nickname = event.source.split('!')[0]
        ping_message = ' '.join(args)
        members = shuffled_members(remove=nickname)
        message = f"{nickname}: {ping_message} /cc {members}"
        conn.privmsg(event.target, message)


class GitLabLink():
    """Print information about GitLab URLs."""

    patterns = [
        r'(?:https://)?(?P<host>[^/]+)/(?P<project>cki-project/[^ ]*)/-/{}/(?P<iid>\d+)',
        r'(?P<project>[^\s]+){}(?P<iid>\d+)',
        # Epics are group level
        r'(?:https://)?(?P<host>[^/]+)/groups/(?P<project>cki-project)/-/{}/(?P<iid>\d+)',
    ]

    objects = {
        'merge_request': ['merge_requests', '!'],
        'issue': ['issues', '#'],
        'epic': ['epics', '&'],
    }

    def _get_patterns_iterator(self):
        """Yield the patterns to check at."""
        for obj, identifiers in self.objects.items():
            for identifier in identifiers:
                for pattern in self.patterns:
                    pattern = pattern.format(identifier)
                    yield obj, pattern

    def __call__(self, conn, event):
        """Process an IRC message."""
        text = event.arguments[0].strip()

        for obj, pattern in self._get_patterns_iterator():
            for match in re.finditer(pattern, text):
                method = getattr(self, f'process_{obj}')
                kwargs = {'host': None}  # host is not always present on the pattern
                kwargs.update(match.groupdict())
                with misc.only_log_exceptions():
                    method(conn, event, **kwargs)

    @staticmethod
    def _search_mr_on_gitlab_projects(project, merge_request):
        """Search through known Gitlab instances for the project."""
        if '/' not in project:
            project = f'cki-project/{project}'
        results = []

        hosts = yaml.safe_load(os.environ.get('GITLAB_TOKENS', '{}')).keys()
        for host in hosts:
            try:
                gl_project = GitLabLink._get_cached_project(host, project)
                if gl_project.archived:
                    continue
                gl_mr = gl_project.mergerequests.get(int(merge_request))
                results.append((gl_project, gl_mr))
            except gitlab.GitlabGetError:
                pass

        if len(results) > 1:
            open_mrs = [r for r in results if r[1].state == 'opened']
            if len(open_mrs) == 1:
                return open_mrs

        return results

    @staticmethod
    @lru_cache(maxsize=None)
    # lru_cache is ok here as static methods don't suffer from the issues described at
    # https://rednafi.github.io/reflections/dont-wrap-instance-methods-with-functoolslru_cache-decorator-in-python.html
    def _get_cached_project(host, project):
        """Get Gitlab project."""
        gl_instance = get_instance(f'https://{host}')
        return gl_instance.projects.get(project)

    @staticmethod
    @lru_cache(maxsize=None)
    # lru_cache is ok here as static methods don't suffer from the issues described at
    # https://rednafi.github.io/reflections/dont-wrap-instance-methods-with-functoolslru_cache-decorator-in-python.html
    def _get_cached_group(host, group):
        """Get Gitlab group."""
        gl_instance = get_instance(f'https://{host}')
        return gl_instance.groups.get(group)

    @staticmethod
    def process_merge_request(conn, event, host, project, iid):
        """Print information about a GitLab merge request."""
        LOGGER.info('Processing GitLab MR %s %s %s',
                    host, project, iid)
        if host:
            gl_project = GitLabLink._get_cached_project(host, project)
            results = [(
                gl_project, gl_project.mergerequests.get(int(iid))
            )]
        else:
            results = GitLabLink._search_mr_on_gitlab_projects(project, iid)

        for gl_project, gl_mr in results:
            msg = str(MergeRequest(gl_project, gl_mr))
            conn.privmsg(event.target, msg)

    @staticmethod
    def process_issue(conn, event, host, project, iid):
        """Process Gitlab issue."""
        LOGGER.info('Processing GitLab Issue %s %s %s',
                    host, project, iid)

        # No need to handle issues on other instances
        host = host or 'gitlab.com'
        gl_project = GitLabLink._get_cached_project(host, project)

        issue = Issue(
            gl_project,
            gl_project.issues.get(int(iid))
        )

        conn.privmsg(event.target, str(issue))

    @staticmethod
    def process_epic(conn, event, host, project, iid):
        """Process Gitlab epic."""
        LOGGER.info('Processing GitLab Epic %s %s %s',
                    host, project, iid)

        # No need to handle issues on other instances
        host = host or 'gitlab.com'
        gl_group = GitLabLink._get_cached_group(host, project)

        epic = Epic(
            gl_group,
            gl_group.epics.get(int(iid))
        )

        conn.privmsg(event.target, str(epic))

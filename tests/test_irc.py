"""Chatbot message relaying tests."""
import unittest

import responses

from irc_bot import irc


class TestIrcBot(unittest.TestCase):
    """ Test IRC message relay."""

    @responses.activate
    def test_send_message(self):
        """Test _send_message."""

        webhook_url = 'https://hooks.slack.com/services/m0Ock1ng/w3b/h0Oks'
        message = "This is an example of a message 😊"

        expected_json = {
            "text": message,
            "unfurl_links": False,
            "unfurl_media": False,
        }
        expected_resp = responses.add(responses.POST, webhook_url, json=expected_json)

        irc_bot = irc.IrcBot(slack_webhook_url=webhook_url)
        irc_bot._send_message(message)  # pylint: disable=protected-access

        self.assertEqual(
            expected_resp.call_count, 2,
            'Expected 2 calls, one for the constructor warning when '
            'the bot is online and one for the actual message')
